octave-chebfun-archive
----------------------

This is an old version of Chebfun which I worked on in 2012 with
an idea to get it running on Octave, there were quite a few issues,
some fixed, some not.  Very brief notes on that in `PORT.txt`

There is now a project to do this Octave port properly, so I make
this old code available, possibly the fixes found in 2012 are still
needed today and this speeds up the project a bit.

> Please don't use this code for anything else, it is out of date
> and only half working, just an archive of an earlier partial
> and abandoned attempt at a port!
