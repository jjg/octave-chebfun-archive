function out = subsasgn(varargin)
% Fun SUBSASGN just call the built-in SUBSASGN.

% Copyright 2011 by The University of Oxford and The Chebfun Developers. 
% See http://www.maths.ox.ac.uk/chebfun/ for Chebfun information.

out = builtin('subsasgn',varargin{:});
