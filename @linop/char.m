function s = char(A)
% CHAR  Convert linop to pretty-printed string.

% Copyright 2011 by The University of Oxford and The Chebfun Developers. 
% See http://www.maths.ox.ac.uk/chebfun/ for Chebfun information.

if isempty(A)
  s = '   (empty linop)';
else
  s = '   operating on chebfuns defined on:';
  s = char(s,' ');
  s = char(s,['  ' char(A.fundomain)],' ');

  if all(A.blocksize==[1 1])
    if ~isempty(A.varmat)
      s = char(s,char(A.varmat,A.fundomain),' ');
    end
    if ~isempty(A.oparray)
      s = char(s, '   with functional representation:',' ',...
        ['     ' char(A.oparray)], ' ');
    end
  else
    s = char(s,sprintf('   with %ix%i block definitions',A.blocksize),' ');
  end
    
  if any(A.difforder(:)~=0)
      if numel(A.difforder) == 1
        s = char(s, ['   and differential order ' num2str(A.difforder)],' ');
      else
        dfostr = [];
        dfo = A.difforder; sdfo = size(dfo);
        for j = 1:sdfo(1);
            for k = 1:sdfo(2)
                dfostr = [dfostr ' ' num2str(dfo(j,k))];
            end
            dfostr = [dfostr ' ;'];
        end
        dfostr(end-1:end) = [];
        s = char(s, ['   and differential order [' dfostr, ' ]']);
      end
  end    
  
  if A.numbc > 0
    if A.numbc==1
      s = char(s,['   and ' int2str(A.numbc) ' boundary condition'],' ' );
    else
      s = char(s,['   and ' int2str(A.numbc) ' boundary conditions'],' ' );
    end
  end
end
