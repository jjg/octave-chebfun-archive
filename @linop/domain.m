function dom = domain(A)
% DOMAIN   Domain of function defintion.

% Copyright 2011 by The University of Oxford and The Chebfun Developers. 
% See http://www.maths.ox.ac.uk/chebfun/ for Chebfun information.

dom = A.fundomain;

end